package com.java.coffee;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

public class CoffeeOrderBoard {

    Map<Integer, String> orders = new ConcurrentHashMap<>();
    Random random = new Random();
    int number = 1;

    private List<String> addNames() {
        List<String> names = new ArrayList<>();
        names.add("Nick");
        names.add("Elena");
        names.add("Jhon");
        names.add("Pit");
        names.add("Olena");
        names.add("Den");
        names.add("Rob");
        names.add("Alice");
        names.add("Hurry");
        names.add("Kate");
        return names;
    }

    private Order selectOrder() {
        return new Order(addNames().get(random.nextInt(10)));
    }

    public Runnable add() {
        return () -> {
            try {
                for (int i = 0; i < 10; i++) {
                    Thread.sleep(random.nextInt(1000));
                    orders.put(number++, selectOrder().getName());
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };
    }

    public  Runnable deliver() {
        return () -> {
            try {
                for (int i = 0; i < 10; i++) {
                    Thread.sleep(1200);
                    orders.remove(random.nextInt(orders.size()));
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };
    }

    public  void print() {
        for (Map.Entry<Integer, String> maps : orders.entrySet()) {
            System.out.println(maps.getKey() + " " + maps.getValue());
        }
    }
}
