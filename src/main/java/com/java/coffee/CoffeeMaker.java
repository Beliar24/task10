package com.java.coffee;

import lombok.SneakyThrows;

public class CoffeeMaker {

    @SneakyThrows
    public static void main(String[] args) {
        CoffeeOrderBoard coffee = new CoffeeOrderBoard();

        Thread thread = new Thread(coffee.add());
        Thread thread1 = new Thread(coffee.deliver());
        thread.start();
        thread1.start();
        thread.join();
        thread1.join();

        coffee.print();

        // The program running approximately 10-12 seconds
    }
}
