package com.java.coffee;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class CoffeeOrderBoardTest {


    @SneakyThrows
    @Test
    void shouldBeCorrectSize() {
        CoffeeOrderBoard coffee = new CoffeeOrderBoard();

        Thread thread = new Thread(coffee.add());
        Thread thread1 = new Thread(coffee.deliver());
        thread.start();
        thread1.start();
        thread.join();
        thread1.join();

        assertTrue(coffee.orders.size() < 10);
        assertTrue(coffee.orders.size() > 1);
    }

    @SneakyThrows
    @Test
    void shouldBeContainCorrectName() {
        CoffeeOrderBoard coffee = new CoffeeOrderBoard();

        List<String> names = new ArrayList<>();
        names.add("Nick");
        names.add("Elena");
        names.add("Jhon");
        names.add("Pit");
        names.add("Olena");
        names.add("Den");
        names.add("Rob");
        names.add("Alice");
        names.add("Hurry");
        names.add("Kate");

        Thread thread = new Thread(coffee.add());
        Thread thread1 = new Thread(coffee.deliver());
        thread.start();
        thread1.start();
        thread.join();
        thread1.join();

        int count = 0;

        for (Map.Entry<Integer, String> maps : coffee.orders.entrySet()) {
            for (int i = 0; i < names.size() - 1; i++) {
                if (maps.getValue().equals(names.get(i))) {
                    count++;
                }
            }
        }

        assertTrue(count < 10);
        assertTrue(count > 1);
    }

}